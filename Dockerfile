# Simple usage with a mounted data directory:
# > docker build -t cgalaxy .
# > docker run -it -p 46657:46657 -p 46656:46656 -v ~/.cgalaxyd:/root/.cgalaxyd -v ~/.cgalaxycli:/root/.cgalaxycli cgalaxy cgalaxyd init
# > docker run -it -p 46657:46657 -p 46656:46656 -v ~/.cgalaxyd:/root/.cgalaxyd -v ~/.cgalaxycli:/root/.cgalaxycli cgalaxy cgalaxyd start
FROM golang:alpine AS build-env

# Set up dependencies
ENV PACKAGES curl make git libc-dev bash gcc linux-headers eudev-dev python3

# Set working directory for the build
WORKDIR /go/src/github.com/thesepehrm/cgalaxy

# Add source files
COPY . .

# Alpine Support for CosmWasm
ADD https://github.com/CosmWasm/wasmvm/releases/download/v0.13.0/libwasmvm_muslc.a /lib/libgo_cosmwasm_muslc.a
RUN sha256sum /lib/libgo_cosmwasm_muslc.a | grep 39dc389cc6b556280cbeaebeda2b62cf884993137b83f90d1398ac47d09d3900

# Install minimum necessary dependencies, build Cosmos SDK, remove packages
RUN apk add --no-cache $PACKAGES && \
    BUILD_TAGS=muslc make install

# Final image
FROM alpine:edge

ENV CGALAXY /cgalaxy

# Install ca-certificates
RUN apk add --update ca-certificates build-base

WORKDIR /root

# Install bash
RUN apk add --no-cache bash

COPY --from=build-env /go/bin/cgalaxyd /usr/bin/cgalaxyd
COPY --from=build-env /go/bin/cgalaxycli /usr/bin/gcalaxycli

EXPOSE 26656 26657 1317 9090

# Run cgalaxyd by default, omit entrypoint to ease using container with cgalaxycli
CMD ["cgalaxyd", "version"]