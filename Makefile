PACKAGES=$(shell go list ./... | grep -v '/simulation')

VERSION := $(shell echo $(shell git describe --tags) | sed 's/^v//')
COMMIT := $(shell git log -1 --format='%H')

ldflags = -X github.com/cosmos/cosmos-sdk/version.Name=CGalaxy \
	-X github.com/cosmos/cosmos-sdk/version.ServerName=cgalaxyd \
	-X github.com/cosmos/cosmos-sdk/version.ClientName=cgalaxycli \
	-X github.com/cosmos/cosmos-sdk/version.Version=$(VERSION) \
	-X github.com/cosmos/cosmos-sdk/version.Commit=$(COMMIT) 

BUILD_FLAGS := -tags "$(BUILD_TAGS)" -ldflags '$(ldflags)'

all: install

install: go.sum
		@echo "--> Installing galaxyd & galaxycli"
		@go install -mod=readonly $(BUILD_FLAGS) ./cmd/cgalaxyd
		@go install -mod=readonly $(BUILD_FLAGS) ./cmd/cgalaxycli

go.sum: go.mod
		@echo "--> Ensure dependencies have not been modified"
		GO111MODULE=on go mod verify

test:
	@go test -mod=readonly $(PACKAGES)
