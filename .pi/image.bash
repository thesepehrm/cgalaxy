#!/bin/bash

set -euo pipefail

# This install script runs inside the chroot of your image builder.

# Connect to the internet by configuring DNS
mv /etc/resolv.conf /etc/resolv.conf.bk
echo 'nameserver 8.8.8.8' > /etc/resolv.conf
echo 'nameserver 1.1.1.1' >> /etc/resolv.conf

# Update packages and install Zerotier
apt update
apt install -y curl wget jq apt-transport-https gnupg gnupg-agent software-properties-common
wget http://download.zerotier.com/debian/buster/pool/main/z/zerotier-one/zerotier-one_1.4.6_arm64.deb
dpkg -i zerotier-one_1.4.6_arm64.deb
apt install -y

# Set Up cgalaxy
chmod +x /usr/bin/cgalaxyd /usr/bin/cgalaxycli
mkdir -p /cgalaxy/config
systemctl enable cgalaxyd